﻿using MediatR;
using WebApiBaseLibrary.Responses;

namespace WebApiBaseLibrary;

public interface IQueryHandler<in TRequest, TResponse> : IRequestHandler<TRequest, IResponse<TResponse>>
    where TRequest : IRequest<IResponse<TResponse>>
{
}
