﻿using WebApiBaseLibrary.Enums;

namespace WebApiBaseLibrary.Responses;

public interface IResponse<out TResult> : IResponse
{
    new TResult? Result { get; }
}

public interface IResponse
{
    ResponseStatus Status { get; }

    object? Result { get; }
    string? Message { get; }
}
