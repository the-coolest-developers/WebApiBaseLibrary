﻿using WebApiBaseLibrary.Enums;

namespace WebApiBaseLibrary.Responses;

public record class Response<T>(ResponseStatus Status) : Response(Status), IResponse<T>
{
    public new T? Result { get; init; }
}

public record class Response(ResponseStatus Status) : IResponse
{
    public object? Result { get; init; }

    public string? Message { get; init; }
}
