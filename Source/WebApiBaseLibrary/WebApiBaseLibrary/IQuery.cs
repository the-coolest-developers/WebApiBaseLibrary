﻿using MediatR;
using WebApiBaseLibrary.Responses;

namespace WebApiBaseLibrary
{
    public interface IQuery<out TResult> : IRequest<IResponse<TResult>>
    {
    }
}
