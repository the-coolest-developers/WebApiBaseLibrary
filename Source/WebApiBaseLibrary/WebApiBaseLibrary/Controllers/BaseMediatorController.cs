﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApiBaseLibrary.ResponseMappers;
using WebApiBaseLibrary.Responses;

namespace WebApiBaseLibrary.Controllers;

public class BaseMediatorController : BaseController
{
    protected IMediator Mediator { get; }

    public BaseMediatorController(IMediator mediator, IResponseMapper responseMapper)
        : base(responseMapper)
    {
        Mediator = mediator;
    }

    public Task<IActionResult> SendToMediatorAsync<TResult>(
        IRequest<IResponse<TResult>> request,
        CancellationToken cancellationToken = default)
    {
        return ResponseMapper.ExecuteAndMapStatusAsync(Mediator.Send(request, cancellationToken));
    }
}