﻿using Microsoft.AspNetCore.Mvc;
using WebApiBaseLibrary.ResponseMappers;

namespace WebApiBaseLibrary.Controllers;

public class BaseController : ControllerBase
{
    protected IResponseMapper ResponseMapper { get; }

    public BaseController(IResponseMapper responseMapper)
    {
        ResponseMapper = responseMapper;
    }
}
