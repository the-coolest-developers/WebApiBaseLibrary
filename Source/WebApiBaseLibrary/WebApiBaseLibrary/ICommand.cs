﻿using MediatR;

namespace WebApiBaseLibrary;

public interface ICommand : IQuery<Unit>
{
}
