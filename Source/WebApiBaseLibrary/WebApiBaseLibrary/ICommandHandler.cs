﻿using MediatR;
using WebApiBaseLibrary.Responses;

namespace WebApiBaseLibrary;
public interface ICommandHandler<TCommand> : IRequestHandler<TCommand, IResponse<Unit>>
    where TCommand : ICommand
{
}