﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApiBaseLibrary.Enums;
using WebApiBaseLibrary.Responses;

namespace WebApiBaseLibrary.ResponseMappers;

public interface IResponseMapper
{
    IActionResult ExecuteAndMapStatus<TResult>(IResponse<TResult> response);

    Task<IActionResult> ExecuteAndMapStatusAsync<TResult>(IResponse<TResult> response);
    Task<IActionResult> ExecuteAndMapStatusAsync<TResult>(Task<IResponse<TResult>> responseTask);

    object GetMessageResponse<TResult>(IResponse<TResult> response);

    int GetStatusCode(ResponseStatus responseStatus);
}
